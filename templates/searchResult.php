<?php foreach ($result as $arr): ?>
    <div class="search_item">
        <p><a class="search_link" href="<?php echo $arr['link']; ?>" target="_blank"
              rel="nofollow"><?php echo $arr['linkText']; ?></a></p>
        <p class="search-caption"><?php echo $arr['caption']; ?></p>
        <span class="search-description"><?php echo $arr['description']; ?></span>
    </div>
<?php endforeach; ?>