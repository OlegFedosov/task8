<DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title><?php echo $title; ?></title>

<link href="css/style.css" rel="stylesheet">
<!-- Bootstrap -->
<link href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="vendor/twbs/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
    <div class="container">
        <div>
            <h2>Search Form</h2>
        </div>
        <div class="<?php echo $message['type'] == 2 ? 'error' : 'message'; ?>">
            <strong><?php echo $message['text']; ?></strong>
        </div>
    
        <form method="post" name="form">
            <div class="form-group">
                <label for="name">Searh</label>
                <input type="text" class="form-control" id="search" value="<?php echo $search; ?>"
                       name="search" placeholder="Enter text">
            </div>
            
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

        <?php echo $content; ?>
    </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="vendor/twbs/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
