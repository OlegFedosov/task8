<?php 

class Model
{ 

    public function setMessage($message, $type = 1)
    {
        $_SESSION['GLOBAL_MESSAGE']['text'] = $message;
        $_SESSION['GLOBAL_MESSAGE']['type'] = in_array($type, [1, 2]) ? $type : 1;
        if ($type = 2)
        {
            return false;
        }

    }

    public function getMessage() 
    {
        if (isset($_SESSION['GLOBAL_MESSAGE']['text']))
        {
            $mes = $_SESSION['GLOBAL_MESSAGE'];
            unset($_SESSION['GLOBAL_MESSAGE']);
            return $mes;
        }
        return false;
    }

    public function wordsForSearch($string)
    {
        if (isset($string) and (mb_strlen(trim($string)) > 3))
        {
            return preg_replace("/\s+/", "+", $string);
        }
        return $this->setMessage('Enter mote than 3 words for search', 2);
    }
    
    public function fileGetContentsCurl($url)
    {
        $ch = curl_init();
        if (!$ch)
        {
            return $this->setMessage('Cannot get content from file', 2);
        }
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);
        if (!$data)
        {
            return $this->setMessage('Cannot get content from file', 2);
        }
    
        $data = (iconv('windows-1251', 'utf-8', $data)); 
        if (!$data)
        {
            return $this->setMessage('Cannot get expected encoding', 2);
        }
        return $data;
    }

    
    function parsText($text) 
	{
		$dom = new DomDocument();
        //clear warnings  
        libxml_use_internal_errors(true);
	    $dom->loadHTML($text, 0);
	    $xpath = new DomXPath( $dom );

        // Don't want to search by class name or tag name + class name!!!!!!!!
        $linksObj = $xpath->query("//cite");
        $descObj = $xpath->query("//span[@class='st']");
        $linkTextObj = $xpath->query("//h3[@class='r']/a");

        // Don't want to search by data-attr of tag!!!!!!!!
        //$linkObj = $xpath->query("//h3[@class='r']/a/@href");
        //foreach ($linkObj as $val)
        //{
        //    $links[] = $val->textContent;
        //}

        if ((count($linksObj) == count($descObj)) and (count($linksObj) == count($linkTextObj)))
        {
            $i = 0;
            foreach ($descObj as $val)
            {
                $data[$i++]['description'] = $val->textContent;
            }

            $i = 0;
            foreach ($linkTextObj as $val)
            {
                $data[$i++]['linkText'] = $val->textContent;
            }

            $i = 0;
            foreach ($linksObj as $val)
            {
                $link = $val->textContent;
                if (strpos($link , '//') === false)
                {
                    $link = '//' . $link;
                }
                $data[$i]['caption'] = $val->textContent;
                $data[$i++]['link'] = $link;
            }

            return $data;
        }

        return $this->setMessage('Something went wrong', 2);
	}
    
}
