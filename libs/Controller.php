<?php

class Controller
{
    private $model;
    private $view;
    private $content;
    private $title;
    private $search;

    public function __construct()
    {
        $this->model = new Model();
        $this->view = new View(TEMPLATE_MAIN);
        $this->title = 'Search';
        if(isset($_POST['search']))
        {
            $this->search();
        }
        else
        {
            $this->pageDefault();
        }
    }

    private function search()
    {   
        $view = new View('searchResult');
        $this->search = trim(htmlspecialchars(strip_tags($_POST['search'])));
        if ($stringQuery = $this->model->wordsForSearch($this->search))
        {
            $content = $this->model->fileGetContentsCurl(SEARCH_URL . $stringQuery);
            $result = $this->model->parsText($content);
            if (count($result))
            {
                $this->content = $view->showTpl(['result' => $result]);
            }
            else
            {
                $this->model->setMessage('0 items found');
                $this->pageDefault();
            }
        }
        else
        {
            $this->model->setMessage('Something went wrong', 2);
            $this->pageDefault();
        }
    }

    private function pageDefault()
    {
        $this->content = null;
    }

    public function render()
    {
        $title = $this->title;
        $content = $this->content;
        $message = $this->model->getMessage();
        $search = $this->search;
        return $this->view->showTpl(compact('title', 'search', 'content', 'message'));
    }
}
